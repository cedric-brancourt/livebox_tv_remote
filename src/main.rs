use dioxus::prelude::*;
use std::net::Ipv4Addr;

const PWR: ButtonSpec = ButtonSpec {
    label: "on/off",
    code: 116,
    icon: "fa-power-off",
};
const VOL_DOWN: ButtonSpec = ButtonSpec {
    label: "vol-",
    code: 114,
    icon: "fa-volume-low",
};
const MUTE: ButtonSpec = ButtonSpec {
    label: "Mute",
    code: 113,
    icon: "fa-volume-xmark",
};
const VOL_UP: ButtonSpec = ButtonSpec {
    label: "vol+",
    code: 115,
    icon: "fa-volume-high",
};

const RWD: ButtonSpec = ButtonSpec {
    label: "backward",
    code: 168,
    icon: "fa-backward-fast",
};
const PLAY: ButtonSpec = ButtonSpec {
    label: "play",
    code: 164,
    icon: "fa-play",
};
const FWD: ButtonSpec = ButtonSpec {
    label: "forward",
    code: 159,
    icon: "fa-forward-fast",
};

const SEVEN: ButtonSpec = ButtonSpec {
    label: "7",
    code: 519,
    icon: "fa-7",
};
const EIGHT: ButtonSpec = ButtonSpec {
    label: "8",
    code: 520,
    icon: "fa-8",
};
const NINE: ButtonSpec = ButtonSpec {
    label: "9",
    code: 521,
    icon: "fa-9",
};

const FOUR: ButtonSpec = ButtonSpec {
    label: "4",
    code: 516,
    icon: "fa-4",
};
const FIVE: ButtonSpec = ButtonSpec {
    label: "5",
    code: 517,
    icon: "fa-5",
};
const SIX: ButtonSpec = ButtonSpec {
    label: "6",
    code: 518,
    icon: "fa-6",
};

const ONE: ButtonSpec = ButtonSpec {
    label: "1",
    code: 513,
    icon: "fa-1",
};
const TWO: ButtonSpec = ButtonSpec {
    label: "2",
    code: 514,
    icon: "fa-2",
};
const THREE: ButtonSpec = ButtonSpec {
    label: "3",
    code: 515,
    icon: "fa-3",
};

const ZERO: ButtonSpec = ButtonSpec {
    label: "0",
    code: 512,
    icon: "fa-0",
};

const UP: ButtonSpec = ButtonSpec {
    label: "⬆",
    code: 103,
    icon: "fa-arrow-up",
};
const DOWN: ButtonSpec = ButtonSpec {
    label: "⬇",
    code: 108,
    icon: "fa-arrow-down",
};
const LEFT: ButtonSpec = ButtonSpec {
    label: "⬅",
    code: 105,
    icon: "fa-arrow-left",
};
const RIGHT: ButtonSpec = ButtonSpec {
    label: "➡",
    code: 106,
    icon: "fa-arrow-right",
};

const OK: ButtonSpec = ButtonSpec {
    label: "Ok",
    code: 352,
    icon: "fa-check",
};
const BCK: ButtonSpec = ButtonSpec {
    label: "Back",
    code: 158,
    icon: "fa-delete-left",
};
const MENU: ButtonSpec = ButtonSpec {
    label: "Menu",
    code: 139,
    icon: "fa-bars",
};

const CHAN_UP: ButtonSpec = ButtonSpec {
    label: "ch+",
    code: 402,
    icon: "fa-chevron-up",
};
const CHAN_DOWN: ButtonSpec = ButtonSpec {
    label: "ch-",
    code: 403,
    icon: "fa-chevron-down",
};

const REC: ButtonSpec = ButtonSpec {
    label: "REC",
    code: 167,
    icon: "fa-circle",
};
const VOD: ButtonSpec = ButtonSpec {
    label: "VOD",
    code: 393,
    icon: "fa-icons",
};

const TV_DECODER_IP: Ipv4Addr = Ipv4Addr::new(192, 168, 1, 10);

#[derive(PartialEq)]
struct ButtonSpec<'a> {
    code: usize,
    label: &'a str,
    icon: &'a str,
}

fn main() {
    dioxus_web::launch(App);
}

#[allow(non_snake_case)]
fn App(cx: Scope) -> Element {
    cx.render(rsx! {
        div {
            style: "display: flex; flex-direction: column",
            Row {
                keys: vec![PWR, VOL_DOWN, MUTE, VOL_UP]
            }
            Row {
                keys: vec![RWD, PLAY, FWD]
            }
            Row {
                keys: vec![SEVEN, EIGHT, NINE]
            }
            Row {
                keys: vec![FOUR, FIVE, SIX]
            }
            Row {
                keys: vec![ONE, TWO, THREE]
            }
            Row {
                keys: vec![ZERO]
            }
            Row {
                keys: vec![UP]
            }
            Row {
                keys: vec![LEFT, RIGHT]
            }
            Row {
                keys: vec![DOWN]
            }
            Row {
                keys: vec![OK,BCK, MENU]
            }
            Row {
                keys: vec![CHAN_UP, CHAN_DOWN]
            }
            Row {
                keys: vec![REC,VOD]
            }
        }
    })
}

#[derive(PartialEq, Props)]
struct RowProps<'a> {
    keys: Vec<ButtonSpec<'a>>,
}

#[allow(non_snake_case)]
fn Row<'a>(cx: Scope<'a, RowProps<'a>>) -> Element<'a> {
    cx.render(rsx! {
         div {
            style: "display: flex; flex-direction: row; justify-content: space-evenly",
            for btn_spec in cx.props.keys.iter() {
               rsx! {
                    button {
                        class: "pure-button",
                        style: "flex-grow: 1;",
                        onclick:  |_| send_code(btn_spec.code),
                        i {
                            class: "fa-solid {btn_spec.icon}",
                            style: "font-size: 120%"
                        }
                        span {
                            visibility: "hidden",
                            "{btn_spec.label}"
                        }
                    }
                }
            }
        }

    })
}

fn send_code(code: usize) {
    let url = format!(
        "http://{}:8080/remoteControl/cmd?operation=01&key={}&mode=0",
        TV_DECODER_IP, code
    );
    let request = ehttp::Request::get(url);

    ehttp::fetch(request, move |result: ehttp::Result<ehttp::Response>| {
        println!("Status code: {:?}", result.unwrap().status);
    })
}
